/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaRegistroController implements Initializable {

    @FXML
    private PasswordField repitaSenha;
    @FXML
    private Label mensagem;
    @FXML
    private PasswordField senha;
    @FXML
    private Button registrarBT;
    @FXML
    private TextField nomeUsuarioTF;
    @FXML
    private Button voltarBT;
    @FXML
    private TextField cpf;
    @FXML
    private TextField idade;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

    @FXML
    private void finalizarCadastro(ActionEvent event) {
        Registro c = new Registro();
        c.setNomeUsuario(nomeUsuarioTF.getText());
        c.setSenha(senha.getText());
        c.setCpf(Integer.parseInt(cpf.getText()));
        c.setIdade(Integer.parseInt(idade.getText()));
        c.insert();
        mensagem.setText("Cadastrado de usuario efetuado");
                   
        cpf.setText("");
        nomeUsuarioTF.setText("");
        senha.setText("");
        idade.setText("");
        repitaSenha.setText("");
    }

    @FXML
    private void voltar(ActionEvent event) throws IOException {
        URL u = getClass().getResource("FXMLDocument.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }
    
}
