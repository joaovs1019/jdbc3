package jdbc3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Produto {
    private int idProduto,quantidade;
    private double valor;
    private String nomeProduto;
    private String categoria;
    
    
    
    public boolean insert(){
    Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO PRODUTO (ID_PRODUTO, nome_produto, preco, categoria,quantidade) VALUES (SEQ_PRODUTO_ID_PRODUTO.nextval,?,?,?,?)";
        

        try {
            String generatedColumns[] = {"ID_PRODUTO"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL,generatedColumns);
            preparedStatement.setString(1, this.nomeProduto);
            preparedStatement.setDouble(2, this.valor);
            preparedStatement.setString(3,this.categoria);
            preparedStatement.setInt(4,this.quantidade);
            
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setIdProduto(chaveGerada);
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    
    public static ArrayList<Produto> getAll() {
        String selectSQL = "SELECT * FROM PRODUTO";
        ArrayList<Produto> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Produto p = new Produto();
                p.setNomeProduto(rs.getString("nome_produto"));
                p.setValor(rs.getDouble("preco"));
                p.setCategoria(rs.getString("categoria"));
                p.setIdProduto(rs.getInt("ID_PRODUTO"));
                p.setQuantidade(rs.getInt("quantidade"));
                lista.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE PRODUTO SET nome_produto=?, preco=?, categoria=?,quantidade =?  WHERE ID_PRODUTO=?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nomeProduto);
            ps.setDouble(2, this.valor);
            ps.setString(3, this.categoria);
            ps.setInt(5, this.idProduto);
            ps.setInt(4, this.quantidade);
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM PRODUTO WHERE ID_PRODUTO = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.idProduto);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }




    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    
    }

    

