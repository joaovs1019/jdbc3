
package jdbc3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Registro {
    
    private String nomeUsuario;
    private String senha;
    private int cpf;
    private int idade;
    private int idCliente;
    
    public boolean insert(){
    Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO cliente (ID_CLIENTE, NOME_CLIENTE, CPF, IDADE, SENHA) VALUES (SEQ_CLIENTE_ID_CLIENTE.nextval,?,?,?,?)";
        

        try {
            String generatedColumns[] = {"ID_CLIENTE"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL,generatedColumns);
            preparedStatement.setString(1, this.nomeUsuario);
            preparedStatement.setString(4, this.senha);
            preparedStatement.setInt(2,this.cpf);
            preparedStatement.setInt(3,this.idade);
            
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setIdCliente(chaveGerada);
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
          
    
}
