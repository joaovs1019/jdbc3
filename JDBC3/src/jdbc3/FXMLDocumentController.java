/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField nomeUsuarioTF;
    @FXML
    private Button entrarBT;
    @FXML
    private Button cadastrarBT;
    @FXML
    private PasswordField senhaTF;
    @FXML
    private Label mensagem;
    
    
    
      
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void entrar(ActionEvent event) throws IOException {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        String selectSQL ="SELECT * FROM cliente WHERE NOME_CLIENTE=? and senha=?";
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nomeUsuarioTF.getText());
            ps.setString(2, senhaTF.getText());
         rs = ps.executeQuery();
         if(rs.next()){
            JDBC3.DadosDoUser.setUsuarioLogado(nomeUsuarioTF.getText());
            URL u = getClass().getResource("TelaInicial.fxml");
            AnchorPane root=FXMLLoader.load(u);
            Scene s=new Scene(root);
            JDBC3.setScene(s);
            
         }else{
             mensagem.setText("usuario ou senha incorreto(s)");
             System.out.println("senha ou usuario incorretos");
         }
    } catch (SQLException e) {
        e.printStackTrace();
    }finally{
        c.desconecta();
        }
    }

    @FXML
    private void Cadastrar(ActionEvent event) throws IOException {
        URL u = getClass().getResource("telaRegistro.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }

   
    
}
