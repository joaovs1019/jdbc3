
package jdbc3;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;


public class ProdutoController implements Initializable {

    @FXML
    private TextField nomeTF;
    @FXML
    private TextField precoTF;
    @FXML
    private RadioButton acaoRB;
    @FXML
    private ToggleGroup categoriaTG;
    @FXML
    private RadioButton aventuraRB;
    @FXML
    private RadioButton terrorRB;
    @FXML
    private RadioButton kidsRB;
    @FXML
    private Button cadastrarBT;
    @FXML
    private Button atualizarBT;
    @FXML
    private Button deletarBT;
    @FXML
    private TableView<Produto> tabela;
    @FXML
    private TableColumn<Produto, String> nomeCol;
    @FXML
    private TableColumn<Produto, Double> precoCol;
    @FXML
    private TableColumn<Produto, String> categoriaCol;
    @FXML
    private RadioButton rpgRB;
    @FXML
    private RadioButton outroRB;
    
    private ObservableList<Produto> produtosList;
        
        
        ArrayList<Produto> produtos = new ArrayList<Produto>();
    @FXML
    private TableColumn<Produto, Integer> quantidadeCol;
    @FXML
    private TextField quantidade;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        atualizaDados();
    }    

    @FXML
    private void cadastrar(ActionEvent event) {
        Produto p = new Produto();
        p.setNomeProduto(nomeTF.getText());
        p.setValor(Double.valueOf(precoTF.getText()));
        p.setQuantidade(Integer.valueOf(quantidade.getText()));
        if(acaoRB.isSelected()){
            p.setCategoria("Ação");
        }
        if(aventuraRB.isSelected()){
            p.setCategoria("Aventura");
        }
        if(terrorRB.isSelected()){
            p.setCategoria("Terror/Suspense");
        }
        if(kidsRB.isSelected()){
            p.setCategoria("Kids");
        }
        if(rpgRB.isSelected()){
            p.setCategoria("RPG");
        }
        if(outroRB.isSelected()){
            p.setCategoria("Outro");
        }
        
        p.insert();

        produtos.add(p);
                   
        //mensagem.setText("Dado Foi Cadastrado Corretamente");
        limparTF();
        atualizaDados();
    }

    @FXML
    private void atualizar(ActionEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
        tabela.getSelectionModel().getSelectedItem().setNomeProduto(nomeTF.getText());
        tabela.getSelectionModel().getSelectedItem().setValor(Double.valueOf(precoTF.getText()));
        tabela.getSelectionModel().getSelectedItem().setQuantidade(Integer.valueOf(quantidade.getText()));
        if(acaoRB.isSelected()){
            tabela.getSelectionModel().getSelectedItem().setCategoria("Açao");
        }
        else if(aventuraRB.isSelected()){
            tabela.getSelectionModel().getSelectedItem().setCategoria("Aventura");
        }
        else if(terrorRB.isSelected()){
            tabela.getSelectionModel().getSelectedItem().setCategoria("Terror/Suspense");
        }
        else if(kidsRB.isSelected()){
            tabela.getSelectionModel().getSelectedItem().setCategoria("Kids");
        }
        else if(rpgRB.isSelected()){
            tabela.getSelectionModel().getSelectedItem().setCategoria("RPG");
        }
        else if(outroRB.isSelected()){
            tabela.getSelectionModel().getSelectedItem().setCategoria("Outro");
        }
        
        tabela.getSelectionModel().getSelectedItem().update();
        limparTF();
       // habilitarTudo();
        atualizaDados();
        }
    }

    @FXML
    private void deletar(ActionEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
        tabela.getSelectionModel().getSelectedItem().delete();
        }
    }
    
    @FXML
    private void selecionado(MouseEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
            deletarBT.setDisable(false);
            atualizarBT.setDisable(false);
            cadastrarBT.setDisable(true);
            
            
            Produto produtoSelecionado = tabela.getSelectionModel().getSelectedItem();
            nomeTF.setText(produtoSelecionado.getNomeProduto());
            precoTF.setText(String.valueOf(produtoSelecionado.getValor()));
            quantidade.setText(String.valueOf(produtoSelecionado.getQuantidade()));
        }
    }
    
     private void atualizaDados(){
      produtosList= tabela.getItems();
      produtosList.clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));      
        precoCol.setCellValueFactory(new PropertyValueFactory<>("valor")); 
        categoriaCol.setCellValueFactory(new PropertyValueFactory<>("categoria"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        for(Produto p : Produto.getAll()){

           produtosList.add(p);
        } 
    }

    public void limparTF(){
        nomeTF.setText("");
        precoTF.setText("");
        quantidade.setText("");
        cadastrarBT.setDisable(false);
        tabela.getSelectionModel().clearSelection();
    }

    @FXML
    private void voltar(ActionEvent event) throws IOException {
        URL u = getClass().getResource("TelaInicial.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }
    
}
