/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;


public class TelaInicialController implements Initializable {

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrarProduto(ActionEvent event) throws IOException {
        URL u = getClass().getResource("produto.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }

    @FXML
    private void carrinho(ActionEvent event) throws IOException {
        URL u = getClass().getResource("Carrinho.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }
    

    

    @FXML
    private void sair(ActionEvent event) throws IOException {
        URL u = getClass().getResource("FXMLDocument.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }
    
}
