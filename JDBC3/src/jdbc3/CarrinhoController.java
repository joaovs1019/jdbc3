/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;


public class CarrinhoController implements Initializable {

    @FXML
    private TableView<Produto> catalogoTabela;
    @FXML
    private TableColumn<Produto, String> nomeCCol;
    @FXML
    private TableColumn<Produto, Double> precoCCol;
    @FXML
    private TableColumn<Produto, String> catCCol;
    @FXML
    private TableView<Produto> carrinhoTabela;
    @FXML
    private TableColumn<Produto, String> nomeCTCol;
    @FXML
    private TableColumn<Produto, String> precoCTCol;
    @FXML
    private TextField totalTF;
    @FXML
    private ComboBox<String> pagamentoMB;
    @FXML
    private Button finalizarCompraBT;
    @FXML
    private Button addCarrinhoBT;
    @FXML
    private Button removerCarinhoBT;
    
    private ObservableList<Produto> produtosList;
    private ObservableList<Produto> carrinhoList;
    private ObservableList<String> tiposList;
    @FXML
    private TableColumn<Produto, Integer> quantidadeCol;

   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizaDados();
        tiposList = pagamentoMB.getItems();
        tiposList.add("Dinheiro");
        tiposList.add("Cartão");
    }    

    

    @FXML
    private void finalizarCompra(ActionEvent event) {
        if(!carrinhoList.isEmpty() && !tiposList.isEmpty()){
        
           
           
            for (Produto produto : produtosList) {
                produto.update();
                carrinhoList.clear();
                totalTF.setText("");
            }
        }
    }

    @FXML
    private void addCarrinho(ActionEvent event) {
        if(catalogoTabela.getSelectionModel().getSelectedItem() != null && catalogoTabela.getSelectionModel().getSelectedItem().getQuantidade() > 0){
           JDBC3.escreva("O Usuario acresentou o produto "+ catalogoTabela.getSelectionModel().getSelectedItem().getIdProduto()+" ao carrinho\n");
            carrinhoList.add(catalogoTabela.getSelectionModel().getSelectedItem());
           catalogoTabela.getSelectionModel().getSelectedItem().setQuantidade(catalogoTabela.getSelectionModel().getSelectedItem().getQuantidade() - 1);
           catalogoTabela.refresh();
           
           
           Double valortotal;
           valortotal = 0.0;
            for (Produto produto : carrinhoList) {
                valortotal+=produto.getValor();
                
            }
            totalTF.setText(String.valueOf(valortotal));
        }
    }

    @FXML
    private void removerCarinho(ActionEvent event) {
        if(carrinhoTabela.getSelectionModel().getSelectedItem() != null){
            JDBC3.escreva("O Usuario removeu o produto "+ carrinhoTabela.getSelectionModel().getSelectedItem().getIdProduto()+" do carrinho\n");
        carrinhoTabela.getSelectionModel().getSelectedItem().setQuantidade(carrinhoTabela.getSelectionModel().getSelectedItem().getQuantidade() + 1);
        carrinhoList.remove(carrinhoTabela.getSelectionModel().getSelectedItem());
           
           catalogoTabela.refresh();
           
           
           
           Double valortotal;
           valortotal = 0.0;
            for (Produto produto : carrinhoList) {
                valortotal+=produto.getValor();
                
            }
            totalTF.setText(String.valueOf(valortotal));
        }
    }

    @FXML
    private void voltar(ActionEvent event) throws IOException {
        URL u = getClass().getResource("TelaInicial.fxml");
        AnchorPane root=FXMLLoader.load(u);
        Scene s=new Scene(root);
        JDBC3.setScene(s);
    }
    
    private void atualizaDados(){
        produtosList= catalogoTabela.getItems();
        produtosList.clear();
        carrinhoList= carrinhoTabela.getItems();
        carrinhoList.clear();
        nomeCCol.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));      
        precoCCol.setCellValueFactory(new PropertyValueFactory<>("valor")); 
        catCCol.setCellValueFactory(new PropertyValueFactory<>("categoria"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        
        nomeCTCol.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));      
        precoCTCol.setCellValueFactory(new PropertyValueFactory<>("valor")); 
        for(Produto p : Produto.getAll()){

           produtosList.add(p);
        } 
    }
    
}
