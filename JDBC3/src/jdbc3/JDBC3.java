/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Aluno
 */
public class JDBC3 extends Application {

    public static DadosDoUser DadosDoUser = new DadosDoUser();

    private static Stage stage;
    private static Logger l = new Logger();
    
    static void setScene(Scene scene) {
        stage.setScene(scene);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        AnchorPane root=FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
         JDBC3.stage=stage;
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest((event) -> {
           System.exit(0);
        });
    }
    
    public static void escreva(String str){
        l.escreve(str);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
