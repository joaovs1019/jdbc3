
/* Drop Triggers */

DROP TRIGGER TRI_cliente_id_cliente;
DROP TRIGGER TRI_item_venda_id_item_venda;
DROP TRIGGER TRI_produto_id_produto;
DROP TRIGGER TRI_venda_id_venda;



/* Drop Tables */

DROP TABLE item_venda CASCADE CONSTRAINTS;
DROP TABLE venda CASCADE CONSTRAINTS;
DROP TABLE cliente CASCADE CONSTRAINTS;
DROP TABLE produto CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE SEQ_cliente_id_cliente;
DROP SEQUENCE SEQ_item_venda_id_item_venda;
DROP SEQUENCE SEQ_produto_id_produto;
DROP SEQUENCE SEQ_venda_id_venda;




/* Create Sequences */

CREATE SEQUENCE SEQ_cliente_id_cliente INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_item_venda_id_item_venda INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_produto_id_produto INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_venda_id_venda INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE cliente
(
	id_cliente number NOT NULL,
	nome_cliente varchar2(50),
	cpf number,
	idade  number,
	PRIMARY KEY (id_cliente)
);


CREATE TABLE item_venda
(
	id_item_venda number NOT NULL,
	quantidade number,
	id_produto number NOT NULL,
	id_venda number NOT NULL,
	PRIMARY KEY (id_item_venda)
);


CREATE TABLE produto
(
	id_produto number NOT NULL,
	nome_produto varchar2(50),
	preco number,
	categoria varchar2(50),
	PRIMARY KEY (id_produto)
);


CREATE TABLE venda
(
	id_venda number NOT NULL,
	valor_total number,
	id_cliente number NOT NULL,
	PRIMARY KEY (id_venda)
);



/* Create Foreign Keys */

ALTER TABLE venda
	ADD FOREIGN KEY (id_cliente)
	REFERENCES cliente (id_cliente)
;


ALTER TABLE item_venda
	ADD FOREIGN KEY (id_produto)
	REFERENCES produto (id_produto)
;


ALTER TABLE item_venda
	ADD FOREIGN KEY (id_venda)
	REFERENCES venda (id_venda)
;



/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_cliente_id_cliente BEFORE INSERT ON cliente
FOR EACH ROW
BEGIN
	SELECT SEQ_cliente_id_cliente.nextval
	INTO :new.id_cliente
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_item_venda_id_item_venda BEFORE INSERT ON item_venda
FOR EACH ROW
BEGIN
	SELECT SEQ_item_venda_id_item_venda.nextval
	INTO :new.id_item_venda
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_produto_id_produto BEFORE INSERT ON produto
FOR EACH ROW
BEGIN
	SELECT SEQ_produto_id_produto.nextval
	INTO :new.id_produto
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_venda_id_venda BEFORE INSERT ON venda
FOR EACH ROW
BEGIN
	SELECT SEQ_venda_id_venda.nextval
	INTO :new.id_venda
	FROM dual;
END;

/




